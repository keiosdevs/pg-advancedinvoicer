<?php namespace Keios\PaymentGatewayInvoicer;

use Backend\Facades\Backend;
use Keios\DocumentHub\Classes\DocumentCreator;
use Keios\Documenthub\Models\DocumentType;
use Keios\DocumentHub\Models\Template;
use Keios\PaymentGateway\Events\OrderPaid;
use Keios\PaymentGatewayInvoicer\Classes\InvoiceRenderModelBuilder;
use Keios\PaymentGatewayInvoicer\Classes\InvoiceTitleGenerator;
use Keios\Paymentgatewayinvoicer\Models\CountryOverride;
use Keios\PaymentGatewayInvoicer\Models\Settings;
use Keios\PaymentGatewayInvoicer\Templates\InvoiceTemplate;
use October\Rain\Exception\ValidationException;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use System\Classes\PluginBase;
use Event;

/**
 * PaymentGatewayInvoicer Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * @var array
     */
    public $require = [
        'Keios.PaymentGateway',
        'Keios.DocumentHub',
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'PaymentGateway Invoicer',
            'description' => 'Provides invoicing for PG',
            'author'      => 'Keios',
            'icon'        => 'icon-leaf',
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Invoicing settings',
                'description' => 'Set up your company details for invoicing',
                'category'    => 'E-Commerce',
                'icon'        => 'icon-cog',
                'class'       => 'Keios\PaymentGatewayInvoicer\Models\Settings',
                'order'       => 500,
                //                'permissions' => ['users.access_settings'], // todo
            ],
        ];
    }

    public function register()
    {
        $this->app['events']->listen(
            OrderPaid::class,
            function (OrderPaid $event) {
                $settings = Settings::instance();
                $builder = new InvoiceRenderModelBuilder($event, $settings);
                $typeId = Template::where('id', $settings->template)->first()->document_type_id;
                $currentCount = DocumentType::where('id', $typeId)->lockForUpdate()->first()->count;
                if (is_null($currentCount)) {
                    throw new \Exception('missing type');
                }
                $newCount = $currentCount + 1;
                $titleGenerator = new InvoiceTitleGenerator($event);
                $title = $titleGenerator->generate($newCount);
                $date = $titleGenerator->addDescription('date');
                try {
                    $renderModel = $builder->build($title, $date);
                } catch (InvalidOptionsException $ex) {
                    throw new ValidationException(
                        [
                            'err' => 'Either user profile or issuer settings are missing required data: '.$ex->getMessage(
                                ),
                        ]
                    );
                } catch (MissingOptionsException $ex) {
                    throw new ValidationException(
                        [
                            'err' => 'Either user profile or issuer settings are missing required data: '.$ex->getMessage(
                                ),
                        ]
                    );
                } catch (\Exception $ex) {
                    throw $ex;
                }

                \DB::beginTransaction();

                try {
                    $description = $titleGenerator->addDescription($settings->description);
                    $user = $builder->getUser();
                    try {
                        $templateId = CountryOverride::where('country_id', $user->country->id)->firstOrFail()->template_id;
                    } catch (\Exception $e)
                    {
                        $templateId = $settings->template;
                    }
                    $docMaker = new DocumentCreator($user, $templateId, $renderModel);
                    $document = $docMaker->create('pdf', false);
                    $document->title = $title; //todo assign id from $document
                    $document->description = $description;
                    $document->save();
                    DocumentType::where('id', $typeId)->update(['count' => $newCount]);
                } catch (\Exception $e) {
                    \DB::rollback();
                    throw $e; //todo
                }

                \DB::commit();
            }
        );
    }

    public function boot()
    {
        Template::registerTemplate(InvoiceTemplate::class);
        Event::listen(
            'backend.menu.extendItems',
            function ($manager) {
                $manager->addSideMenuItems(
                    'Keios.DocumentHub',
                    'documenthub',
                    [
                        'comments' => [
                            'label' => 'keios.paymentgatewayinvoier::lang.strings.overrides',
                            'icon'  => 'icon-globe',
                            'code'  => 'overrides',
                            'owner' => 'Keios.DocumentHub',
                            'url'   => Backend::url('keios/paymentgatewayinvoicer/countryoverrides'),
                        ],
                    ]
                );
            }
        );
    }
}