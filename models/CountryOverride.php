<?php namespace Keios\Paymentgatewayinvoicer\Models;

use Illuminate\Support\Facades\DB;
use Keios\DocumentHub\Models\Template;
use Keios\ProUser\Models\Country;
use Model;

/**
 * CountryOverride Model
 */
class CountryOverride extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_paymentgatewayinvoicer_country_overrides';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    public $belongsTo = [
        'country'  => 'Keios\ProUser\Models\Country',
        'template' => 'Keios\DocumentHub\Models\Template',
    ];

    public function getCountryIdOptions()
    {
        return Country::lists('name', 'id');
    }

    public function getTemplateIdOptions()
    {
        return $availableTemplates = DB::table((new Template)->getTable())->lists('title', 'id');
    }

}