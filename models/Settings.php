<?php namespace Keios\PaymentGatewayInvoicer\Models;

use Keios\DocumentHub\Models\Template;
use October\Rain\Database\Model;
use Illuminate\Support\Facades\DB;

/**
 * Settings Model
 */
class Settings extends Model
{

    public $implement = ['System.Behaviors.SettingsModel'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * A unique code of settings
     */
    public $settingsCode = 'keios::paymentgatewayinvoicer.settings';

    /**
     * Reference to field configuration
     */
    public $settingsFields = 'fields.yaml';

    public function getTemplateOptions()
    {
        return $availableTemplates = DB::table((new Template)->getTable())->lists('title', 'id');
    }

}