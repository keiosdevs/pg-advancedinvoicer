<?php namespace Keios\PaymentGatewayInvoicer\Templates;

use Keios\DocumentHub\Classes\TwigTemplate;
use Keios\DocumentHub\Models\Template;
use Keios\MoneyRight\Currency;
use Keios\MoneyRight\Money;
use Keios\PaymentGateway\ValueObjects\Cart;
use Keios\RechargeShop\Classes\RechargeShopItem;

/**
 * Class InvoiceTemplate
 *
 * @package Keios\DocumentHub\Templates
 */
class InvoiceTemplate extends TwigTemplate
{
    /**
     * @var array
     */
    protected $requiredData = [
        // issuer part
        'issuer_company_name',
        'issuer_issuer',
        'issuer_address',
        'issuer_city',
        'issuer_zip',
        'issuer_vat_no',
        'issuer_register_no',
        'issuer_country',
        // buyer part
        'buyer_first_name',
        'buyer_last_name',
        //'buyer_vat_number',
        //'buyer_register_number',
        'buyer_street',
        'buyer_house_number',
        'buyer_flat_number',
        'buyer_zip',
        'buyer_city',
        // order part
        'cart',
    ];

    /**
     * @var array
     */
    protected $exampleData = [
        // issuer part
        'issuer_company_name'   => 'Keios',
        'issuer_issuer'         => 'John Smith',
        'issuer_address'        => 'John Paul 2 Street',
        'issuer_city'           => 'Wadowice',
        'issuer_zip'            => '23-333',
        'issuer_vat_no'         => '14882137',
        'issuer_register_no'    => '21371488',
        'issuer_country'        => 'Poland',

        // buyer part
        'buyer_first_name'      => 'Larry',
        'buyer_last_name'       => 'Page',
        'buyer_company_name'    => 'Google',
        'buyer_vat_number'      => null,
        'buyer_register_number' => null,
        'buyer_street'          => 'Mountain View',
        'buyer_house_number'    => '1',
        'buyer_flat_number'     => null,
        'buyer_zip'             => '3243423423',
        'buyer_city'            => 'Palo Alto',

        // order part
        'cart'                  => [],
    ];

    /**
     * @var array
     */
    public static $renderVariables = [
        // issuer part
        'issuer_company_name',
        'issuer_issuer',
        'issuer_address',
        'issuer_city',
        'issuer_zip',
        'issuer_vat_no',
        'issuer_register_no',
        'issuer_country',
        // buyer part
        'buyer_first_name',
        'buyer_last_name',
        'buyer_company_name',
        'buyer_vat_number',
        'buyer_register_number',
        'buyer_street',
        'buyer_house_number',
        'buyer_flat_number',
        'buyer_zip',
        'buyer_city',
        // order part
        'cart',
    ];

    /**
     * @var string
     */
    public static $translationKey = 'keios.paymentgatewayinvoicer::lang.templates.invoicetemplate';


    public function __construct(Template $templateModel, array $data = null, $previewMode = false)
    {
        parent::__construct($templateModel, $data, $previewMode);

        if ($this->isPreview) {
            $cart = new Cart('USD');
            $cart->add(new RechargeShopItem('Recharge', 1, new Money(23, new Currency('USD')), 23, []));
            $this->exampleData['cart'] = $cart;
        }
    }

    /**
     *
     */
    protected function getTemplateData()
    {
        $this->preparedData = $this->validatedData;
    }
}