<?php
return [
    'templates' => [
        'testtemplate' => 'Testing template',
        'invoicetemplate' => 'Basic invoice template',
    ],
    'countryoverrides' => [
        'menu_label' => 'Country Overrides',
        'delete_confirm' => 'Do you really want to delete this countryoverride?',
        'return_to_list' => 'Return to Country Overrides',
        'delete_selected_confirm' => 'Delete the selected country overrides?',
        'delete_selected_success' => 'Successfully deleted the selected country overrides.',
        'delete_selected_empty' => 'There are no selected :name to delete.',
    ],
    'countryoverride' => [
        'new' => 'New Country Override',
        'list_title' => 'Manage Country Overrides',
        'label' => 'Country Override',
        'create_title' => 'Create Country Override',
        'update_title' => 'Edit Country Override',
        'preview_title' => 'Preview Country Override',
    ],
];