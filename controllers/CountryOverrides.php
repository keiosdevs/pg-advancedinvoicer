<?php namespace Keios\Paymentgatewayinvoicer\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;

/**
 * Country Overrides Back-end Controller
 */
class CountryOverrides extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Keios.Paymentgatewayinvoicer', 'paymentgatewayinvoicer', 'countryoverrides');
    }

    /**
     * Deleted checked countryoverrides.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $countryoverrideId) {
                if (!$countryoverride = CountryOverride::find($countryoverrideId)) continue;
                $countryoverride->delete();
            }

            Flash::success(Lang::get('keios.paymentgatewayinvoicer::lang.countryoverrides.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('keios.paymentgatewayinvoicer::lang.countryoverrides.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}