<?php namespace Keios\PaymentGatewayInvoicer\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateInvoicerSettingsTable extends Migration
{

    public function up()
    {
        Schema::create('keios_paymentgatewayinvoicer_invoicer_settings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('keios_paymentgatewayinvoicer_invoicer_settings');
    }

}
