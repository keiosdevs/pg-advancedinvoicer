<?php namespace Keios\Paymentgatewayinvoicer\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreateCountryOverridesTable extends Migration
{

    public function up()
    {
        Schema::create('keios_paymentgatewayinvoicer_country_overrides', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('country_id')->index();
            $table->integer('template_id')->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('keios_paymentgatewayinvoicer_country_overrides');
    }

}
