<?php
/**
 * Created by Keios Solutions.
 * User: Jakub Zych
 * Date: 11/19/15
 * Time: 2:06 PM
 */

namespace Keios\PaymentGatewayInvoicer\Classes;


use Carbon\Carbon;
use Keios\DocumentHub\Models\Document;
use Keios\PaymentGateway\Events\OrderPaid;
use Keios\PaymentGatewayInvoicer\Models\Settings;
use Keios\ProUser\Models\Country;

/**
 * Class InvoiceTitleGenerator
 *
 * @package Keios\PaymentGatewayInvoicer\Classes
 */
class InvoiceTitleGenerator
{
    /**
     * @var Document
     */
    private $order;

    /**
     * InvoiceTitleGenerator constructor.
     *
     * @param OrderPaid $order
     */
    public function __construct(OrderPaid $order)
    {
        $this->order = $order->getModel();
    }

    /**
     * @return string
     */
    public function generate($count)
    {
        $settings = Settings::instance();

        $format = $settings->numbering;

        $string = $this->replaceVar($format, $count);

        return $string;
    }


    /**
     * @param string|null $mode
     *
     * @return string
     */
    public function addDescription($mode = null)
    {

        if ($mode == 'date') {
            $rawDate = Carbon::now();
            $description = $rawDate->toDateString();
        } else {
            $amount = $this->order->amount;
            $nAmount = $this->order->native_amount;
            $amountString = $amount->getAmountBasic().' '.$amount->getCurrency()->getIsoCode();
            $nAmountString = $nAmount->getAmountBasic().' '.$nAmount->getCurrency()->getIsoCode();

            $description = 'Invoice value: '.$amountString;

            if ($nAmount->getCurrency() != $amount->getCurrency()) {
                $description = $description.' ('.$nAmountString.')';
            }
        }

        return $description;
    }

    /**
     * @param $format
     *
     * @return string
     */
    private function replaceVar($format, $count)
    {
        $day = $this->getDay();
        $simpleDay = $this->getSimpleDay();
        $number = $this->getNumber($count);
        $simpleMonth = $this->getSimpleMonth();
        $month = $this->getMonth();
        $year = $this->getYear();
        $fullYear = $this->getFullYear();

        $result = str_replace('SIMPLE_NUMBER', $count, $format);
        $result = str_replace('NUMBER', $number, $result);
        $result = str_replace('SIMPLE_DAY', $simpleDay, $result);
        $result = str_replace('DAY', $day, $result);
        $result = str_replace('SIMPLE_MONTH', $simpleMonth, $result);
        $result = str_replace('MONTH', $month, $result);
        $result = str_replace('FULL_YEAR', $fullYear, $result);
        $result = str_replace('YEAR', $year, $result);

        // todo - done for NumberOne, this is just wrong.
        $country = Country::where('id', $this->order->user->country_id)->first();
        if ($country->code == 'RO') {
            $result = str_replace('IE', 'RO', $result);
        }

        return $result;
    }

    /**
     * @param $simpleNumber
     *
     * @return string
     */
    private function getNumber($simpleNumber)
    {
        $settings = Settings::instance();
        $length = $settings->number_length;
        $sLength = strlen((string)$simpleNumber);
        $zeros = $length - $sLength;
        $zeroString = '';
        for ($count = 0; $count < $zeros; $count++) {
            $zeroString = $zeroString.'0';
        }

        if (strlen($simpleNumber) == 1 || $length > 0) {
            return $zeroString.$simpleNumber;
        }

        return $simpleNumber;
    }


    /**
     * @return string
     */
    private function getDay()
    {
        $date = $this->order->updated_at;
        if (strlen($date->day) == 1) {
            return '0'.$date->day;
        }

        return $date->day;
    }


    /**
     * @return mixed
     */
    private function getSimpleDay()
    {
        $date = $this->order->updated_at;

        return $date->day;
    }

    /**
     * @return string
     */
    private function getSimpleMonth()
    {
        $date = $this->order->updated_at;

        return $date->month;
    }

    /**
     * @return string
     */
    private function getMonth()
    {
        $date = $this->order->updated_at;
        if (strlen($date->month) == 1) {
            return '0'.$date->month;
        }

        return $date->month;
    }

    /**
     * @return string
     */
    private function getYear()
    {
        $date = $this->order->updated_at;

        return substr($date->year, 2);
    }

    /**
     * @return string
     */
    private function getFullYear()
    {
        $date = $this->order->updated_at;

        return $date->year;
    }


}