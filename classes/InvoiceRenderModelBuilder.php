<?php
/**
 * Created by PhpStorm.
 * User: Łukasz Biały
 * URL: http://keios.eu
 * Date: 11/18/15
 * Time: 6:35 PM
 */

namespace Keios\PaymentGatewayInvoicer\Classes;

use Keios\PaymentGateway\Core\Order;
use Keios\PaymentGateway\Models\Order as OrderModel;
use Keios\PaymentGateway\Events\OrderPaid;
use Keios\PaymentGatewayInvoicer\Models\Settings;
use Keios\ProUser\Models\User;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InvoiceRenderModelBuilder
{
    /**
     * @var Order
     */
    private $order;

    /**
     * @var OrderModel
     */
    private $orderModel;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Settings
     */
    private $settings;

    public function __construct(OrderPaid $event, Settings $settings)
    {
        $this->order = $event->getObject();
        $this->orderModel = $this->order->getModel();
        $this->user = $this->orderModel->user;
        $this->settings = $settings;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return OptionsResolver
     */
    public function getOptionsResolver()
    {
        $resolver = new OptionsResolver();

        $resolver->setDefaults(
            [
                // issuer part
                'issuer_company_name'   => 'Set up company name!',
                'issuer_issuer'         => 'Set up issuer!',
                'issuer_address'        => 'Set up address!',
                'issuer_city'           => 'Set up city!',
                'issuer_zip'            => 'Set up zip!',
                'issuer_vat_no'         => 'Set up vat_no!',
                'issuer_register_no'    => 'Set up register_no!',
                'issuer_country'        => 'Set up country!',

                // buyer part
                'buyer_company_name'    => null,
                'buyer_vat_number'      => null,
                'buyer_register_number' => null,
                'buyer_street'          => null,
                'buyer_house_number'    => null,
                'buyer_flat_number'     => null,
                'buyer_zip'             => null,
                'buyer_city'            => null,

                // order part
                'cart'                  => null,

                'invoice_title' => 'Numbering does not work',
                'invoice_date'  => 'Dating does not work',
            ]
        );

        $resolver->setRequired(
            [
                'issuer_company_name',
                'issuer_issuer',
                'issuer_address',
                'issuer_city',
                'issuer_zip',
                'issuer_vat_no',
                'issuer_register_no',
                'issuer_country',
                'cart',
                'order_id',
                'buyer_first_name',
                'buyer_last_name',
                'buyer_street',
                'buyer_house_number',
                'buyer_flat_number',
                'buyer_zip',
                'buyer_city',
                'invoice_title',
                'invoice_date',

            ]
        );

        return $resolver;
    }

    public function build($title, $date)
    {
        $toResolve = [

            // issuer part
            'issuer_company_name'   => $this->settings->company_name,
            'issuer_issuer'         => $this->settings->issuer,
            'issuer_address'        => $this->settings->address,
            'issuer_city'           => $this->settings->city,
            'issuer_zip'            => $this->settings->zip,
            'issuer_vat_no'         => $this->settings->vat_no,
            'issuer_register_no'    => $this->settings->register_no,
            'issuer_country'        => $this->settings->country,

            // buyer part
            'buyer_first_name'      => $this->user->first_name,
            'buyer_last_name'       => $this->user->last_name,
            'buyer_company_name'    => $this->user->company_name,
            'buyer_vat_number'      => $this->user->vat_number,
            'buyer_register_number' => $this->user->register_number,
            'buyer_street'          => $this->user->street,
            'buyer_house_number'    => $this->user->house_number,
            'buyer_flat_number'     => $this->user->flat_number,
            'buyer_zip'             => $this->user->zip,
            'buyer_city'            => $this->user->city,

            // order part
            'cart'                  => $this->order->cart,
            'order_id'              => $this->order->hash_id,

            // custom
            'invoice_title'         => $title,
            'invoice_date'          => $date,
        ];


        $resolver = $this->getOptionsResolver();

        return $resolver->resolve($toResolve);
    }
}